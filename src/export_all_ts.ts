var path = require("path")
var fs = require('fs')
export default class Utils {

    static export_all_ts() {
        let to_path = path.join(Editor.Project.path, "all_ts.txt")
        console.log("export all ts", to_path, Editor.Project.path)

        if (fs.existsSync(to_path)) {
            fs.unlink(to_path, (err: Error) => {
                if (err) console.log(err)
            })
        }
        let assets_path = path.join(Editor.Project.path, "assets")
        this.walk_in_dir(assets_path)

        fs.writeFile(to_path, this.contents, 'utf-8', (err: any) => {
            console.log("export success", to_path)
        })
    }

    static contents: string = ""


    static load_one(_file_path: string) {
        // console.log(_file_path)
        let content = fs.readFileSync(_file_path, 'utf-8')
        // console.log("content", content)
        let sub_path = "//" + _file_path.substr(Editor.Project.path.length + 8) + "\n"
        this.contents += sub_path + content + "\n"
    }

    static walk_in_dir(_dir: string) {
        let files = fs.readdirSync(_dir)
        files.forEach((file: string) => {
            let file_path = path.join(_dir, file)
            let stats = fs.statSync(file_path)
            if (stats.isDirectory()) {
                this.walk_in_dir(file_path)
            } else {
                if (file.endsWith(".ts")) {
                    this.load_one(file_path)
                }
            }
        })
    }

}
